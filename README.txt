Welcome to XDrawChem! (version 1.11.0)

Mostly written by Bryan Herger, bherger@users.sourceforge.net
See acknowledgements below for specific contributions.

This fork of XDrawChem is hosted at https://gitlab.com/yamanq/xdrawchem
Please report bugs and suggest features here.

The original XDrawChem web site is http://xdrawchem.sourceforge.net/

This program is released under the terms of the GNU General Public License.
Portions of the source code are copyright by others.  
Please see the files COPYRIGHT.txt and LICENSE.txt included in this package.

NOTE:  This is a mostly stable version, but probably still has bugs!
       See LICENSE.txt for copyright and limitation of liability.

XDrawChem is a two-dimensional molecule drawing program for Unix
operating systems.  It is similar in functionality to other molecule
drawing programs such as ChemDraw (TM, CambridgeSoft).  It can read
and write MDL Molfiles and CML files to allow sharing between
XDrawChem and other chemistry applications.  XDrawChem has been tested
on Linux, Sun Solaris and SGI IRIX.  XDrawChem was designed with Qt
3.0 or later, available free from https://www.qt.io
XDrawChem also depends on the OpenBabel library, http://openbabel.sf.net/

Please read INSTALL.txt which gives instructions on how to install
XDrawChem.

XDrawChem features a limited manual.  Run the program and press F1 (or 
select Manual under the Help menu) for instructions.

The HISTORY.txt file lists changes made in each revision.

Acknowledgements and many thanks to the following people who have
contributed:
Yaman Qalieh
Thomas LeClerc
Masao Kawamura
Guy Brand
Sven Bornemann
Kuznik Nikodem
Ralf Ahlbrink
Eduardo Sanchez
Ronald Bialozyt
Egon Willighagen 
Armando Navarro
Bojan Ivancic
Brett Saunders
Geoffrey Wossum (for autoconf macros, http://autoqt.sf.net/)
Brian Kelley (for Frowns project, http://frowns.sf.net/)
Christian Becke (for pixmaps)
Everyone who works on OpenBabel (http://openbabel.sf.net/)
Vitaly Lipatov and Roman Borisyuk (for Russian language translation)
Daniel Leidert (for German language translation)
Thomas Shattuck
Gerd Fleischer
...and others not listed yet...
