#!/bin/sh

package=xdrawchem
version=$2
untarDir=$(cd ..; tar tzf ${package}_${version}.orig.tar.gz | head -n 1)

wd=$(pwd)

cd ..
tar xzf ${package}_${version}.orig.tar.gz 
mv ${untarDir}/xdrawchem-qt5 ${package}-$version
rm -rf ${untarDir}
tar cJf ${package}_${version}.orig.tar.xz ${package}-$version
rm -f $(readlink ${package}_${version}.orig.tar.gz) ${package}_${version}.orig.tar.gz

echo made ../${package}_${version}.orig.tar.xz and ../${package}-$version
