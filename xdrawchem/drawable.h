// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// drawable.h -- the class that is the parent of all drawable objects

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <QColor>
#include <QList>
#include <QRect>
#include <QString>

#include "dpoint.h"

class Bond;
class Text;

class Drawable : public QObject {
    Q_OBJECT

  public:
    Drawable(QObject *parent = 0);
    virtual Drawable *CloneTo(Drawable *target = nullptr) const; // Clone current data to target
    virtual void Render();                                       // draw this object
    virtual void Edit();                                         // edit this object
    virtual int Type();                                          // return type of object
    virtual DPoint *FindNearestPoint(DPoint *, double &);
    virtual Drawable *FindNearestObject(DPoint *, double &);
    virtual bool Find(DPoint *);
    virtual void addBond(DPoint *, DPoint *, int, int, QColor, bool hl = false);
    virtual void addMolecule(Drawable *);
    virtual void Highlight();
    virtual void Highlight(bool);
    virtual bool Highlighted();
    virtual bool Erase(Drawable *);
    virtual bool isWithinRect(QRect, bool);
    virtual void SelectAll();
    virtual void DeselectAll();
    virtual void Move(double, double);
    virtual void ForceMove(double, double);
    virtual void Rotate(DPoint *, double);
    virtual void Flip(DPoint *, int);
    virtual void Resize(DPoint *, double);
    virtual QRect BoundingBox();
    virtual QList<DPoint *> AllPoints();
    virtual QList<Drawable *> AllObjects();
    virtual QString ToXML(QString);
    virtual QString ToCDXML(QString);
    virtual void FromXML(QString);
    virtual int Members();
    // needed only to merge Molecules; see molecule.cpp, addMolecule()
    virtual Bond *bondsFirst() { return 0; }
    virtual Bond *bondsNext() { return 0; }
    virtual Text *labelsFirst() { return 0; }
    virtual Text *labelsNext() { return 0; }
    // stuff that all Drawables should know about
    static double getAngle(DPoint *, DPoint *);
    double DistanceToLine(DPoint *, DPoint *, DPoint *);
    double DistanceBetween(QPointF, QPointF);
    bool DPointInRect(DPoint *, QRect);
    DPoint *Start() { return start; }
    DPoint *End() { return end; }
    void setStart(DPoint *ns) { start = ns; }
    void setEnd(DPoint *ne) { end = ne; }
    virtual void SetColorIfHighlighted(QColor);
    void SetColor(QColor c) { color = c; }
    QColor GetColor() { return color; }
    QString getID() { return id; }
    void setID(QString x) { id = x; }
    QColor GetColorFromXML(QString);
    void SetColorFromXML(QString);
    void SetStartFromXML(QString);
    void SetEndFromXML(QString);
    bool isInGroup() { return ingroup; }
    void setInGroup(bool x) { ingroup = x; }
    int Thick() { return thick; }
    void setThick(int t) { thick = t; }
    // more or less everything must be protected so derived classes can use them

  protected:
    // highlighted?
    bool highlighted;
    // member of a group?
    bool ingroup;
    // points which define this Drawable (only start needed for TEXT)
    DPoint *start, *end;
    // color
    QColor color;
    // XML ID
    QString id;
    // arrow or bracket style
    int style;
    // which symbol or curvearrow to draw
    QString which;
    // thickness of bond/bracket/line
    int thick;
};

#endif
