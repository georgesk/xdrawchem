// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef NETDIALOG_H
#define NETDIALOG_H

#include <QComboBox>
#include <QDialog>

class QLineEdit;

class NetDialog : public QDialog {
  public:
    NetDialog(QWidget *parent);

    QString getKey() {
        QString ret;
        if (keylist->currentIndex() == 0)
            ret = "name";
        else if (keylist->currentIndex() == 1)
            ret = "cas";
        else if (keylist->currentIndex() == 2)
            ret = "formula";
        return ret;
    }

    QString getValue() { return searchkey->text(); }
    QString getServer() { return serverkey->text(); }
    bool getExact() { return emcheck->isChecked(); }

  private:
    QString fn;
    QComboBox *dblist, *keylist;
    QCheckBox *emcheck;
    QLineEdit *searchkey, *serverkey;
};

#endif
