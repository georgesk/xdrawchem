// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include <QWidget>

class QPaintEvent;

class PreviewWidget : public QWidget {
  public:
    PreviewWidget(QWidget *parent = 0);
    void updateWidget(int a1, int a2, int a3, int a4, int a5, QColor c = Qt::black) {
        type = a1;
        th = a2;
        da = a3;
        _or = a4;
        style = a5;
        color = c;
        update();
    }
    int type, th, da, _or, style;
    QColor color;

  protected:
    void paintEvent(QPaintEvent *);
};

#endif
