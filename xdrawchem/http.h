// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef HTTP_H
#define HTTP_H

#include <QCoreApplication>
#include <QDialog>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslError>
#include <QStringList>
#include <QTimer>
#include <QUrl>

#include <stdio.h>

class QSslError;

QT_USE_NAMESPACE

class HTTP : public QDialog {
    Q_OBJECT
    QNetworkAccessManager *manager;
    QList<QNetworkReply *> currentDownloads;
    QString replyText;
    QString cUrl;
    bool finished;

  public:
    HTTP();
    HTTP(QString);
    void doDownload(const QUrl &url);
    QString Data();
    void execute(QString);

  public slots:
    void downloadFinished(QNetworkReply *reply);
    void slotError(QNetworkReply::NetworkError e);
    void sslErrors(const QList<QSslError> &errors);
};

#endif
