// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// bracket.h -- subclass of Drawable for brackets

#ifndef BRACKET_H
#define BRACKET_H

#include <QColor>
#include <QRect>
#include <QString>

#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class Bracket : public Drawable {
  public:
    Bracket(Render2D *, QObject *parent = 0);
    Bracket *CloneTo(Drawable *target = nullptr) const;
    void Render(); // draw this object
    void Edit();
    int Type();          // return type of object
    bool Find(DPoint *); // does this Bracket contain this DPoint?
    DPoint *FindNearestPoint(DPoint *, double &);
    Drawable *FindNearestObject(DPoint *, double &);
    void setPoints(DPoint *, DPoint *);
    bool isWithinRect(QRect, bool);
    QRect BoundingBox();
    QString ToXML(QString);
    QString ToCDXML(QString);
    void FromXML(QString);
    int Style() { return style; }
    void SetStyle(int s1) { style = s1; }
    void setText(QString s1) { subtext = s1; }
    QString getText() { return subtext; }
    void setFill(bool b1) { fill = b1; }
    bool getFill() { return fill; }
    void setFillColor(QColor fc1) { fillColor = fc1; }
    QColor getFillColor() { return fillColor; }
    void SetFillColorFromXML(QString);

    bool fillable() {
        if (style > 3)
            return true; // see defs.h for BRACKET_<types>
        return false;
    }

  private:
    // Renderer
    Render2D *r;
    // subscript text, if applicable
    QString subtext;
    // fill options, where applicable
    bool fill;
    QColor fillColor;
};

#endif
