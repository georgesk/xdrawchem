// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef PEPTDIALOG_H
#define PEPTDIALOG_H

#include <QDialog>
#include <QLineEdit>

class PeptDialog : public QDialog {
    Q_OBJECT

  public:
    PeptDialog(QWidget *parent);
    QString getPeptide();

  public slots:
    void buttonPress(int);

  private:
    QLineEdit *peptide;
};

#endif
