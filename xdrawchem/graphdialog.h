// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef GRAPHDIALOG_H
#define GRAPHDIALOG_H

#include <QColor>
#include <QDialog>
#include <QString>

#include "graphdata.h"
#include "graphwidget.h"

class GraphDialog : public QDialog {
    Q_OBJECT

  public:
    GraphDialog(QWidget *parent, const QString &name);
    void AddPeak(double, QString l1 = "", QString t1 = "");
    void AddPeak(double, QColor, QString l1 = "", QString t1 = "");
    void AddPeak(double, int, QColor, QString l1 = "", QString t1 = "");
    void AddPixmap(QPixmap);

  public slots:
    void SendHelp();

  private:
    GraphWidget *g;
};

#endif
