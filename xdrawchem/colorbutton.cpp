// XDrawChem
// Copyright (C) 2006  Gerd Fleischer <gerdfleischer@gmx.de>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QColor>
#include <QIcon>
#include <QPainter>
#include <QPixmap>
#include <QRect>
#include <QStyle>
#include <QStyleOptionButton>
#include <qdrawutil.h>

#include "colorbutton.h"

ColorButton::ColorButton(QColor color, QWidget *parent) : QPushButton(parent) {
    btnColor = color;
    setColor(btnColor);
}

void ColorButton::paintEvent(QPaintEvent *) {
    QPainter painter(this);

    // First, we need to draw the bevel.
    QStyleOptionButton butOpt;
    initStyleOption(&butOpt);
    style()->drawControl(QStyle::CE_PushButtonBevel, &butOpt, &painter, this);

    // OK, now we can muck around with drawing out pretty little color box
    // First, sort out where it goes
    QRect labelRect = style()->subElementRect(QStyle::SE_PushButtonContents, &butOpt, this);
    int shift = style()->pixelMetric(QStyle::PM_ButtonMargin);
    labelRect.adjust(shift, shift, -shift, -shift);
    int x, y, w, h;
    labelRect.getRect(&x, &y, &w, &h);

    if (isChecked() || isDown()) {
        x += style()->pixelMetric(QStyle::PM_ButtonShiftHorizontal);
        y += style()->pixelMetric(QStyle::PM_ButtonShiftVertical);
    }

    QColor fillCol = isEnabled() ? btnColor : palette().color(backgroundRole());
    qDrawShadePanel(&painter, x, y, w, h, palette(), true, 1, NULL);
    if (fillCol.isValid())
        painter.fillRect(x + 1, y + 1, w - 2, h - 2, fillCol);

    if (hasFocus()) {
        QRect focusRect = style()->subElementRect(QStyle::SE_PushButtonFocusRect, &butOpt, this);
        QStyleOptionFocusRect focusOpt;
        focusOpt.init(this);
        focusOpt.rect = focusRect;
        focusOpt.backgroundColor = palette().window().color();
        style()->drawPrimitive(QStyle::PE_FrameFocusRect, &focusOpt, &painter, this);
    }
}

void ColorButton::initStyleOption(QStyleOptionButton *opt) const {
    opt->init(this);
    opt->text.clear();
    opt->icon = QIcon();
    opt->features = QStyleOptionButton::None;
}

void ColorButton::setColor(QColor color) {
    btnColor = color;
    QPixmap colorIcon = QPixmap(width(), height());
    colorIcon.fill(btnColor);
    setIcon(QIcon(colorIcon));
}
