// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QHBoxLayout>
#include <QSpacerItem>
#include <QVBoxLayout>

#include "application.h"
#include "chemdata.h"
#include "render2d.h"
#include "smilesdialog.h"

void ApplicationWindow::FromSMILES() {
    SmilesDialog i(this);

    if (!i.exec())
        return;
    QString sm = i.getSMILES();

    m_chemData->fromSMILES(sm);
    m_renderer->Inserted();
    m_renderer->update();
}

SmilesDialog::SmilesDialog(QWidget *parent) : QDialog(parent) {
    setWindowTitle(tr("Enter InChI or SMILES string"));

    QVBoxLayout *smilesLayout = new QVBoxLayout();

    QLabel *smilesLabel = new QLabel(tr("Enter InChI or SMILES string:"), this);
    smilesLayout->addWidget(smilesLabel);

    smilesInput = new QLineEdit(this);
    connect(smilesInput, SIGNAL(returnPressed()), this, SLOT(accept()));
    smilesLayout->addWidget(smilesInput);

    QHBoxLayout *btnLayout = new QHBoxLayout();
    QSpacerItem *btnSpacer = new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Minimum);
    btnLayout->addItem(btnSpacer);

    QPushButton *okBtn = new QPushButton(tr("OK"), this);
    connect(okBtn, SIGNAL(clicked()), SLOT(accept()));
    btnLayout->addWidget(okBtn);

    QPushButton *cancelBtn = new QPushButton(tr("Cancel"), this);
    connect(cancelBtn, SIGNAL(clicked()), SLOT(reject()));
    btnLayout->addWidget(cancelBtn);

    smilesLayout->addLayout(btnLayout);
    setLayout(smilesLayout);
}

QString SmilesDialog::getSMILES() { return smilesInput->text(); }
