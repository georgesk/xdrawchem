// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// CDXML_Reader:  XML Reader for CDXML type.

#include <QColor>
#include <QList>
#include <QString>

#include "chemdata.h"
#include "dpoint.h"
#include "xml_reader.h"

// TODO Remove int index
struct ColorTableEntry {
    int index;
    QColor color;
};

struct FontTableEntry {
    QString id;
    QString fam;
};

class CDXML_Object {
  public:
    CDXML_Object() {}
    QString id;       // XML ID
    QString start_id; // id of start and end points for bonds
    QString end_id;
    DPoint *start; // start and end points for non-bonds
    DPoint *end;
    int type;
    int idata1, idata2;
    double ddata1, ddata2;
    QString sdata;
    QColor color;
    QFont font;
};

class CDXML_Reader : public XML_Reader {
  public:
    CDXML_Reader(ChemData *);
    bool ReadFile(QString);

  private:
    // basically, all member functions are private.  Not necessary but
    // probably a good idea
    void ParseDocument(QString);
    void ParseColorTable(QString);
    QColor ParseColor(QString);
    void ParseFontTable(QString);
    FontTableEntry ParseFont(QString);
    void ParsePage(QString);
    void ParseFragment(QString);
    QString ParseText(QString);
    void ParseGraphic(QString);
    void ParseNode(QString);
    void ParseBond(QString);
    DPoint *FindNode(QString);
    void Build();
    // ----
    // data
    // ----
    // color table
    QList<ColorTableEntry> colors;
    // font table
    QList<FontTableEntry> fonts;
    // node list (atoms) and temp object
    QList<DPoint *> nodelist;
    // CDXML object list and temp object
    QList<CDXML_Object *> objectlist;
    // ChemData object we are reading into
    ChemData *c;
    // how many levels of node/fragment are we in
    int nodedepth, fragdepth;
    // for passing data
    double globalx, globaly;
};
