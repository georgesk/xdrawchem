// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// biotools.h -- subclass of Drawable for representing various biochemical
// structures:  membranes, antibodies, etc.

#ifndef BIOTOOLS_H
#define BIOTOOLS_H

#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class Molecule;

class BioTools : public Drawable {
  public:
    BioTools(Render2D *, QObject *parent = 0);
    // BioTools *CloneTo(Drawable *target = nullptr) const;
    void Render(); // draw this object
    int Type();    // return type of object
    void SetWhich(QString);
    QString GetWhich() { return which; }

  private:
    // Renderer
    //  Render2D *r;
};

#endif
