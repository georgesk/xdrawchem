// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef XML_CML_H
#define XML_CML_H

/*
#include <q3ptrlist.h>
#include <qxml.h>
*/

#include <QXmlDefaultHandler>
#include <qxmlstream.h>

#include "bond.h"
#include "dpoint.h"
#include "drawable.h"
#include "render2d.h"

class QString;

// possible states (CMLParser::states)
// set in defs.h
//#define CML_NONE 0
//#define CML_ATOM 1
//#define CML_BOND 2

class CMLParser {
  public:
    CMLParser(Render2D *r1, QXmlStreamReader *x1) : r(r1), xml(x1) {}
    bool parse();
    // bool startDocument();
    bool startElement();
    bool endElement();
    bool characters();
    // bool ignored( const QString & );
    QList<DPoint *> getPoints();
    QList<Bond *> getBonds();

  private:
    QList<DPoint *> localPoints;
    QList<Bond *> localBonds;
    DPoint *tmp_pt, *ep1, *ep2;
    Bond *tmp_bond;
    Render2D *r;
    QXmlStreamReader *xml;
    QString indent, last_builtin;
    int states;
};

#endif
