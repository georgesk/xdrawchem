// XDrawChem
// Copyright (C) 2004-2005  Bryan Herger <bherger@users.sourceforge.net>
// Copyright (C) 2020  Yaman Qalieh <ybq987@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <QGridLayout>
#include <QPixmap>
#include <QPushButton>

#include "defs.h"
#include "graphdata.h"
#include "graphdialog.h"
#include "helpwindow.h"
#include "render2d.h"

// defined in main.cpp
extern QString RingDir;

GraphDialog::GraphDialog(QWidget *parent, const QString &name) : QDialog(parent) {
    setWindowTitle(name);

    QGridLayout *dialogLayout = new QGridLayout();
    setLayout(dialogLayout);

    g = new GraphWidget();
    dialogLayout->addWidget(g, 0, 0, 1, 2);
    //    g->setGeometry( 0, 0, 600, 500 );
    // QPushButton *showhide = new QPushButton("Show/hide molecule", this);
    // showhide->setGeometry(20,320,150,40);
    // showhide->setPalette(QPalette(QPalette::Midlight));
    QPushButton *qprint = new QPushButton(tr("Print"), this);
    connect(qprint, SIGNAL(clicked()), g, SLOT(Print()));
    dialogLayout->addWidget(qprint, 1, 0);

    QPushButton *qexport = new QPushButton(tr("Export Peak List"), this);
    connect(qexport, SIGNAL(clicked()), g, SLOT(Export()));
    dialogLayout->addWidget(qexport, 1, 1);

    QPushButton *qhelp = new QPushButton(tr("Help"), this);
    connect(qhelp, SIGNAL(clicked()), SLOT(SendHelp()));
    dialogLayout->addWidget(qhelp, 2, 0);

    QPushButton *qclose = new QPushButton(tr("Close"), this);
    connect(qclose, SIGNAL(clicked()), SLOT(accept()));
    dialogLayout->addWidget(qclose, 2, 1);

    if (name.contains("13C-NMR") > 0)
        g->setDataType(1);
    if (name.contains("IR") > 0)
        g->setDataType(2);
    if (name.contains("1H-NMR") > 0)
        g->setDataType(3);
}

void GraphDialog::AddPeak(double v1, QString l1, QString t1) {
    AddPeak(v1, QColor(0, 0, 0), l1, t1);
}

void GraphDialog::AddPeak(double v1, QColor c1, QString l1, QString t1) {
    if (v1 < 0)
        return; // negative indicates invalid peak.
    g->AddPeak(v1, c1, l1, t1);
}

void GraphDialog::AddPeak(double v1, int m1, QColor c1, QString l1, QString t1) {
    g->AddPeak(v1, m1, c1, l1, t1);
}

void GraphDialog::AddPixmap(QPixmap p1) { g->AddPixmap(p1); }

void GraphDialog::SendHelp() {
    QString home, topic;

    topic = "spectra.html";
#ifdef UNIX
    home = RingDir + "doc/" + topic;
#else
    home = RingDir + "doc\\" + topic;
#endif
    HelpWindow *help = new HelpWindow(home, ".", 0);

    help->setWindowTitle(QString(XDC_VERSION) + " - Help viewer");
    help->show();
}

// cmake#include "graphdialog.moc"
